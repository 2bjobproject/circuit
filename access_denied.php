
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo TITLE_ENG; ?></title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link href="bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css"/>
    <link href="dist/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <link href="dist/css/AdminLTE.css" rel="stylesheet" type="text/css"/>
    <link href="Styles/pagerStyle.css" rel="stylesheet" type="text/css"/>
    <link href="dist/css/skins/_all-skins.css" rel="stylesheet" type="text/css"/>
    <style type="text/css">
        .div-center-screen {
            position: fixed;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
        }
    </style>
</head>
<body>
<div class="col-sm-8 div-center-screen">
    <div class="alert alert-danger alert-dismissible">
        <h4><i class="icon fa fa-ban"></i> เกิดข้อผิดพลาดบางอย่าง!</h4>
        คุณกำลังพยายามเข้าถึงหน้าที่ไม่ได้รับอนุญาต
    </div>

</div>
</body>
</html>
