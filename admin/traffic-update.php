<?php
session_start();
require_once "../common.inc.php";
if (!is_list_session(array(ADMIN_LEVEL)))
    redirect_to('index.php');

require_once "../connection.inc.php";

$cmd = getIsset("__cmd");
$test = array();
$traffic_box_id = getIsset('__traffic_box_id');
if ($cmd == "save") {
    $value = array(
        "traffic_box_name" => getIsset('__traffic_box_name'),
        "time_trigger_field" => getIsset('__time_trigger_field'),
        "road_number_id" => getIsset('__road_number_id'),
        "stage_trigger_field" => getIsset('__stage_trigger_field'),
        "traffic_box_picture" => uploadFile($_FILES['__file_upload'], getIsset('__traffic_box_picture'), PATH_UPLOAD),
    );
    if ($traffic_box_id == "0") {
        if ($conn->create("mtraffic_box", $value)) {
            redirectTo("traffic.php");
        }

    } else {
        if ($conn->update("mtraffic_box", $value, array("traffic_box_id" => $traffic_box_id))) {
            redirectTo("traffic.php");
        }
    }
}

?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo TITLE_ENG; ?> </title>

    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link href="../bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css"/>
    <link href="../dist/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <link href="../dist/css/AdminLTE.css" rel="stylesheet" type="text/css"/>

    <link href="../dist/css/skins/_all-skins.css" rel="stylesheet" type="text/css"/>
    <link href="../assets/dist/pagination.css" rel="stylesheet">
    <link href="../assets/css/custom.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../assets/css/jquery.datetimepicker.css">
    <style>

        #demo, .paginationjs {
            display: flex;
            flex-direction: row;
            flex-wrap: wrap;
            justify-content: center;
            align-items: center;
        }
    </style>
</head>
<body class="skin-yellow sidebar-mini">
<div class="wrapper">
    <?php include "navbar.php" ?>
    <?php include "sidebar.php" ?>
    <div id="posContain" class="content-wrapper">
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <form class="form-horizontal" id="form_data" name="form_data" method="post"
                          enctype="multipart/form-data">
                        <input id="__cmd" name="__cmd" type="hidden" value="">

                        <div class="col-md-12">
                            <label class="col-sm-3 control-label">
                            </label>
                        </div>
                        <div class="clr"></div>
                        <div class="col-sm-12">
                            <div class="box box-success">
                                <div class="box-header with-border">
                                    <h3 class="box-title">จัดการข้อมูลผไฟจราจร </h3>
                                </div>
                                <div class="box-body">
                                    <input type="hidden" name="__traffic_box_id" id="__traffic_box_id"
                                           class="form-control"
                                           value="0"
                                           required="true" readonly>
                                    <div class="form-group">
                                        <div align="right">
                                            <label class="col-sm-3 control-label">
                                                รูปภาพ :
                                            </label>
                                        </div>
                                        <div class="col-sm-3">
                                            <img id="img-preview" src="" class="image img-thumbnail"
                                                 onerror="src='../assets/img/openBox-512.png'">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">

                                        </label>

                                        <div class="col-sm-5">
                                            <input type="file" name="__file_upload" style="display: none">
                                            <input type="hidden" name="__traffic_box_picture"
                                                   id="__traffic_box_picture">
                                            <a type="button" class="btn btn-success btn-xs upload-logo">อัพโหลด</a>
                                            <a type="button" class="btn btn-danger btn-xs del-logo">ลบไฟล์</a>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div align="right">
                                            <label class="col-sm-3 control-label">
                                                ถนน :
                                            </label>
                                        </div>
                                        <div class="col-sm-5">
                                            <div class="input-group">
                                                <input type="hidden" name="__road_number_id" id="__road_number_id"
                                                       class="form-control"
                                                       value=""
                                                       readonly>
                                                <input type="text" name="__road_number_name" id="__road_number_name"
                                                       class="form-control"
                                                       value="" readonly>
                                                <a href="javascript:goPage('linkhelp.php?__filter=road_number&__action=getRoad_numberById');"
                                                   class="btn btn-default input-group-addon"><i
                                                            class="fa fa-search"></i> </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div align="right">
                                            <label class="col-sm-3 control-label">
                                                ชื่อกล่องไฟจราจร :
                                            </label>
                                        </div>
                                        <div class="col-sm-5">
                                            <input type="text" name="__traffic_box_name" id="__traffic_box_name"
                                                   class="form-control"
                                                   value=""
                                                   onblur="trimValue(this);" required="true">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div align="right">
                                            <label class="col-sm-3 control-label">
                                                Time Trigger :
                                            </label>
                                        </div>
                                        <div class="col-sm-5">
                                            <input type="text" name="__time_trigger_field" id="__time_trigger_field"
                                                   class="form-control"
                                                   value=""
                                                   onblur="trimValue(this);" required="true">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div align="right">
                                            <label class="col-sm-3 control-label">
                                                Stage Trigger :
                                            </label>
                                        </div>
                                        <div class="col-sm-5">
                                            <input type="text" name="__stage_trigger_field" id="__stage_trigger_field"
                                                   class="form-control"
                                                   value=""
                                                   onblur="trimValue(this);" required="true">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div align="right">
                                            <label class="col-sm-3 control-label">
                                            </label>
                                        </div>
                                        <div class="col-sm-5">
                                            <a class="btn btn-success" href="javascript:goSave();">บันทึก</a>
                                            <a class="btn btn-warning" href="javascript:goClear()">เคลียร์</a>
                                            <a class="btn btn-default" href="traffic.php">ย้อนกลับ</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </section>
    </div>
</div>
<?php require_once 'javascript.php'; ?>
<!-- Page script -->
<script>
    $('#menu-traffic-main').addClass('active');
    $('#menu-traffic').addClass('active');

    function helpReturn(value, action) {
        $.ajax({
            url: 'Allservice.php',
            data: {id: value, action: action},
            method: 'GET',
            success: function (result) {
                var data = JSON.parse(result);
                if (action == "getTraffic_boxById") {
                    if (data.traffic_box_id != null) {
                        console.log(data);
                        setValueTraffic_box(data);
                    }
                }
                if (action == "getRoad_numberById") {
                    if (data.road_number_id != null) {
                        console.log(data);
                        setValueRoad_number(data);
                    }
                }
            }
        });
    }

    function setValueTraffic_box(data) {
        with (document.form_data) {
            $("#__traffic_box_id").val(data.traffic_box_id);
            $("#__traffic_box_name").val(data.traffic_box_name);
            $("#__time_trigger_field").val(data.time_trigger_field);
            $("#__stage_trigger_field").val(data.stage_trigger_field);
            $("#__road_number_id").val(data.road_number_id);
            $("#__road_number_name").val(data.road_number_name);
            $("#img-preview").attr('src', '<?php echo PATH_UPLOAD . '/';?>' + data.traffic_box_picture);
        }
    }
    function setValueRoad_number(data) {
        with (document.form_data) {
            $("#__road_number_id").val(data.road_number_id);
            $("#__road_number_name").val(data.road_number_name);

        }
    }
</script>
<script>helpReturn('<?php echo $traffic_box_id;?>', 'getTraffic_boxById')</script>
</body>
</html>


