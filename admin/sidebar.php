<aside class="main-sidebar">
    <section class="sidebar">
        <ul class="sidebar-menu">
            <?php
            switch ($uprofile['level']) {
                case ADMIN_LEVEL:
                    ?>
                    <li id="menu-index">
                        <a href="index.php">
                            <i class="fa fa-home"></i>
                            <span data-toggle="tooltip" title="หน้าหลัก">หน้าหลัก</span>
                        </a>
                    </li>
                    <li class="treeview" id="menu-user">
                        <a href="#">
                            <i class="fa fa-folder-open"></i> <span>ข้อมูลผู้ใช้งานระบบ</span> <i
                                class="fa fa-angle-left pull-right"></i>
                        </a>
                        <ul class="treeview-menu">
                            <li id="menu-employee"><a href="employee.php"><i class="fa fa-folder-open"></i>
                                    จัดการข้อมูลพนักงาน</a></li>
                        </ul>
                    </li>
                    <li class="treeview" id="menu-road-main">
                        <a href="#">
                            <i class="fa fa-folder-open"></i> <span>ข้อมูลถนน</span> <i
                                    class="fa fa-angle-left pull-right"></i>
                        </a>
                        <ul class="treeview-menu">
                            <li id="menu-road"><a href="road.php"><i class="fa fa-folder-open"></i>
                                    จัดการข้อมูลถนน</a></li>
                        </ul>
                    </li>
                    <li class="treeview" id="menu-traffic-main">
                        <a href="#">
                            <i class="fa fa-folder-open"></i> <span>ข้อมูลกล่องไฟจราจร</span> <i
                                    class="fa fa-angle-left pull-right"></i>
                        </a>
                        <ul class="treeview-menu">
                            <li id="menu-traffic"><a href="traffic.php"><i class="fa fa-folder-open"></i>
                                    จัดการข้อมูลกล่องไฟจราจร</a></li>
                        </ul>
                    </li>
                    <li class="treeview" id="menu-formula-main">
                        <a href="#">
                            <i class="fa fa-folder-open"></i> <span>ข้อมูลสูตร</span> <i
                                    class="fa fa-angle-left pull-right"></i>
                        </a>
                        <ul class="treeview-menu">
                            <li id="menu-formula"><a href="formula.php"><i class="fa fa-folder-open"></i>
                                    จัดการข้อมูลสูตร</a></li>
                            <li id="menu-formulad"><a href="formula_detail.php"><i class="fa fa-folder-open"></i>
                                    จัดการข้อมูลรายละเอียดสูตร</a></li>
                        </ul>
                    </li>
                    <li class="treeview" id="menu-formula_detail_traffic-main">
                        <a href="#">
                            <i class="fa fa-folder-open"></i> <span>ข้อมูลรายสูตรกล่องไฟจราจร</span> <i
                                    class="fa fa-angle-left pull-right"></i>
                        </a>
                        <ul class="treeview-menu">
                            <li id="menu-formula_detail_traffic"><a href="formula_detail_traffic.php"><i class="fa fa-folder-open"></i>
                                    จัดการข้อมูลรายสูตรกล่องไฟจราจร</a></li>
                        </ul>
                    </li>

                    <?php
                    break;
            }
            ?>
        </ul>
    </section>
</aside>