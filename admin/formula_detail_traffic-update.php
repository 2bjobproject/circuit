<?php
session_start();
require_once "../common.inc.php";
if (!is_list_session(array(ADMIN_LEVEL)))
    redirect_to('index.php');

require_once "../connection.inc.php";

$cmd = getIsset("__cmd");
$test = array();
$run_no = getIsset('__run_no');
if ($cmd == "save") {
    $value = array(
        "formula_id" => getIsset('__formula_id'),
        "traffic_box_id" => getIsset('__traffic_box_id'),
        "amount_sec" => getIsset('__amount_sec'),

    );
    if ($run_no == "0") {
        if ($conn->create("formula_detail_traffic", $value)) {
            redirectTo("formula_detail_traffic.php");
        }

    } else {
        if ($conn->update("formula_detail_traffic", $value, array("run_no" => $run_no))) {
            redirectTo("formula_detail_traffic.php");
        }
    }
}

?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo TITLE_ENG; ?> </title>

    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link href="../bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css"/>
    <link href="../dist/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <link href="../dist/css/AdminLTE.css" rel="stylesheet" type="text/css"/>

    <link href="../dist/css/skins/_all-skins.css" rel="stylesheet" type="text/css"/>
    <link href="../assets/dist/pagination.css" rel="stylesheet">
    <link href="../assets/css/custom.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../assets/css/jquery.datetimepicker.css">
    <style>

        #demo, .paginationjs {
            display: flex;
            flex-direction: row;
            flex-wrap: wrap;
            justify-content: center;
            align-items: center;
        }
    </style>
</head>
<body class="skin-yellow sidebar-mini">
<div class="wrapper">
    <?php include "navbar.php" ?>
    <?php include "sidebar.php" ?>
    <div id="posContain" class="content-wrapper">
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <form class="form-horizontal" id="form_data" name="form_data" method="post"
                          enctype="multipart/form-data">
                        <input id="__cmd" name="__cmd" type="hidden" value="">

                        <div class="col-md-12">
                            <label class="col-sm-3 control-label">
                            </label>
                        </div>
                        <div class="clr"></div>
                        <div class="col-sm-12">
                            <div class="box box-success">
                                <div class="box-header with-border">
                                    <h3 class="box-title">จัดการข้อมูลสรายละเอียดูตร </h3>
                                </div>
                                <div class="box-body">
                                    <input type="hidden" name="__run_no" id="__run_no" class="form-control"
                                           value="0"
                                           required="true" readonly>
                                    <div class="form-group">
                                        <div align="right">
                                            <label class="col-sm-3 control-label">
                                                สูตร :
                                            </label>
                                        </div>
                                        <div class="col-sm-5">
                                            <div class="input-group">
                                                <input type="hidden" name="__formula_id" id="__formula_id"
                                                       class="form-control"
                                                       value=""
                                                       readonly>
                                                <input type="text" name="__formula_name" id="__formula_name"
                                                       class="form-control"
                                                       value="" readonly>
                                                <a href="javascript:goPage('linkhelp.php?__filter=formula&__action=getFormulaById');"
                                                   class="btn btn-default input-group-addon"><i
                                                            class="fa fa-search"></i> </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div align="right">
                                            <label class="col-sm-3 control-label">
                                                กล่องไฟจราจร :
                                            </label>
                                        </div>
                                        <div class="col-sm-5">
                                            <div class="input-group">
                                                <input type="hidden" name="__traffic_box_id" id="__traffic_box_id"
                                                       class="form-control"
                                                       value=""
                                                       readonly>
                                                <input type="text" name="__traffic_box_name" id="__traffic_box_name"
                                                       class="form-control"
                                                       value="" readonly>
                                                <a href="javascript:goPage('linkhelp.php?__filter=traffic_box_name&__action=getTraffic_boxById');"
                                                   class="btn btn-default input-group-addon"><i
                                                            class="fa fa-search"></i> </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div align="right">
                                            <label class="col-sm-3 control-label">
                                                รูปภาพ :
                                            </label>
                                        </div>
                                        <div class="col-sm-2">
                                            <img id="img-preview" src="" class="image img-thumbnail"
                                                 onerror="src='../assets/img/openBox-512.png'">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div align="right">
                                            <label class="col-sm-3 control-label">
                                                จำนวนวินาที :
                                            </label>
                                        </div>
                                        <div class="col-sm-5">
                                            <input type="text" name="__amount_sec" id="__amount_sec"
                                                   class="form-control"
                                                   value="">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div align="right">
                                            <label class="col-sm-3 control-label">
                                            </label>
                                        </div>
                                        <div class="col-sm-5">
                                            <a class="btn btn-success" href="javascript:goSave();">บันทึก</a>
                                            <a class="btn btn-warning" href="javascript:goClear()">เคลียร์</a>
                                            <a class="btn btn-default" href="formula_detail_traffic.php">ย้อนกลับ</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </section>
    </div>
</div>
<?php require_once 'javascript.php'; ?>
<!-- Page script -->
<script>
    $('#menu-formula_detail_traffic-main').addClass('active');
    $('#menu-formula_detail_traffic').addClass('active');

    function helpReturn(value, action) {
        $.ajax({
            url: 'Allservice.php',
            data: {id: value, action: action},
            method: 'GET',
            success: function (result) {
                var data = JSON.parse(result);
                if (action == "getFormula_detail_trafficById") {
                    if (data.run_no != null) {
                        console.log(data);
                        setValueFormula_detail_traffic(data);
                    }
                }
                if (action == "getTraffic_boxById") {
                    if (data.traffic_box_id != null) {
                        console.log(data);
                        setValueTraffic_box(data);
                    }
                }
                if (action == "getFormulaById") {
                    if (data.formula_id != null) {
                        console.log(data);
                        setValueFormula(data);
                    }
                }
            }
        });
    }

    function setValueFormula_detail_traffic(data) {
        with (document.form_data) {
            $("#__run_no").val(data.run_no);
            $("#__traffic_box_id").val(data.traffic_box_id);
            $("#__amount_sec").val(data.amount_sec);


            $("#__traffic_box_id").val(data.traffic_box_id);
            $("#__traffic_box_name").val(data.traffic_box_name);
            $("#__traffic_box_picture").val(data.traffic_box_picture);
            $("#img-preview").attr('src', '<?php echo PATH_UPLOAD . '/';?>' + data.traffic_box_picture);

            $("#__formula_id").val(data.formula_id);
            $("#__formula_name").val(data.formula_name);
        }
    }
    function setValueTraffic_box(data) {
        with (document.form_data) {
            $("#__traffic_box_id").val(data.traffic_box_id);
            $("#__traffic_box_name").val(data.traffic_box_name);
            $("#__traffic_box_picture").val(data.traffic_box_picture);
            $("#img-preview").attr('src', '<?php echo PATH_UPLOAD . '/';?>' + data.traffic_box_picture);
        }
    }
    function setValueFormula(data) {
        with (document.form_data) {
            $("#__formula_id").val(data.formula_id);
            $("#__formula_name").val(data.formula_name);

        }
    }
</script>
<script>helpReturn('<?php echo $run_no;?>', 'getFormula_detail_trafficById')</script>
</body>
</html>


