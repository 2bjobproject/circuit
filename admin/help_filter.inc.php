<?php
$cmd = getIsset('__filter');
$freturn = getIsset('__freturn');
$filter = array();
$filter['title'] = 'DEFAULT';
$filter['order_by'] = "";

if ($cmd == 'employee') {
    $filter['column'] = array('employee_id' => 'รหัส', 'first_name' => 'ชื่อ', 'last_name' => 'นามสกุล', 'username' => 'ชื่อผู้ใช้', 'phone' => 'เบอร์โทรศัพท์');
    $filter['sql'] = "select * from employee where 1=1 ";
    $filter['key_id'] = 'employee_id';
    $filter['title'] = 'ข้อมูลพนักงานขับรถ';
    $filter['options'] = array('employee_id' => 'รหัส', 'first_name' => 'ชื่อ', 'last_name' => 'นามสกุล', 'username' => 'ชื่อผู้ใช้', 'phone' => 'เบอร์โทรศัพท์');
}
if ($cmd == 'traffic_box_name') {
    $filter['column'] = array('traffic_box_id' => 'รหัส', 'traffic_box_name' => 'ชื่อ');
    $filter['sql'] = "select * from mtraffic_box where 1=1 ";
    $filter['key_id'] = 'traffic_box_id';
    $filter['title'] = 'ข้อมูลกล่องไฟจราจร';
    $filter['options'] = array('traffic_box_id' => 'รหัส', 'traffic_box_name' => 'ชื่อ');
}

if ($cmd == 'formula') {
    $filter['column'] = array('formula_id' => 'รหัส', 'formula_name' => 'ชื่อ');
    $filter['sql'] = "select * from mformula where 1=1 ";
    $filter['key_id'] = 'formula_id';
    $filter['title'] = 'ข้อมูลสูตร';
    $filter['options'] = array('formula_id' => 'รหัส', 'formula_name' => 'ชื่อ');
}

if ($cmd == 'road_number') {
    $filter['column'] = array('road_number_id' => 'รหัส', 'road_number_name' => 'ชื่อ', 'default_duration_time' => 'เวลามาตรฐาน');
    $filter['sql'] = "select * from mroad_number where 1=1 ";
    $filter['key_id'] = 'road_number_id';
    $filter['title'] = 'ข้อมูลถนน';
    $filter['options'] = array('road_number_id' => 'รหัส', 'road_number_name' => 'ชื่อ', 'default_duration_time' => 'เวลามาตรฐาน');
}

if (getIsset('option') != "" && getIsset('keyword') != "") {
    $filter['sql'] = get_filter(getIsset('option'), getIsset('keyword'), $filter['sql']);
}

function get_filter($col, $value, $sql)
{
    return $sql . ' AND ' . $col . ' LIKE \'%' . $value . '%\' ';
}
