<?php
session_start();
require_once('../connection.inc.php');
require_once('../common.inc.php');
$action = getIsset('action');
$result = array();

if ($action == 'getEmployeeById') {
    $id = getIsset('id');
    $result = $conn->queryRaw("select employee.*,employee_type_name from employee
left join employee_type on employee_type.employee_type_id=employee.employee_type_id  where employee_id='$id'", true);
}

if ($action == 'getRoad_numberById') {
    $id = getIsset('id');
    $result = $conn->queryRaw("select * from mroad_number  where road_number_id='$id'", true);
}
if ($action == 'getFormulaById') {
    $id = getIsset('id');
    $result = $conn->queryRaw("select * from mformula  where formula_id='$id'", true);
}

if ($action == 'getFormula_detailById') {
    $id = getIsset('id');
    $result = $conn->queryRaw("select formula_detail.*,road_number_name,formula_name from formula_detail
left join mformula on mformula.formula_id=formula_detail.formula_id
left join mstage on mstage.stage_id=formula_detail.stage_id
left join mroad_number on mroad_number.road_number_id=formula_detail.road_number_id   where formula_detail_no='$id'", true);
}
if ($action == 'getFormula_detail_trafficById') {
    $id = getIsset('id');
    $result = $conn->queryRaw("select formula_detail_traffic.*,traffic_box_name,formula_name,traffic_box_picture from formula_detail_traffic
left join mformula on mformula.formula_id=formula_detail_traffic.formula_id
left join mtraffic_box on mtraffic_box.traffic_box_id=formula_detail_traffic.traffic_box_id   where run_no='$id'", true);
}

if ($action == 'getTraffic_boxById') {
    $id = getIsset('id');
    $result = $conn->queryRaw("select mtraffic_box.*,road_number_name from mtraffic_box 
 left join mroad_number on mroad_number.road_number_id=mtraffic_box.road_number_id where traffic_box_id='$id'", true);
}




echo json_encode($result);
