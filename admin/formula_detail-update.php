<?php
session_start();
require_once "../common.inc.php";
if (!is_list_session(array(ADMIN_LEVEL)))
    redirect_to('index.php');

require_once "../connection.inc.php";

$cmd = getIsset("__cmd");
$test = array();
$formula_detail_no = getIsset('__formula_detail_no');
if ($cmd == "save") {
    $value = array(
        "formula_id" => getIsset('__formula_id'),
        "road_number_id" => getIsset('__road_number_id'),
        "duration_time_start" => getIsset('__duration_time_start'),
        "stage_id" => getIsset('__stage_id'),
        "stage_time" => getIsset('__stage_time'),
        "duration_time_end" => getIsset('__duration_time_end'),

    );
    if ($formula_detail_no == "0") {
        if ($conn->create("formula_detail", $value)) {
            redirectTo("formula_detail.php");
        }

    } else {
        if ($conn->update("formula_detail", $value, array("formula_detail_no" => $formula_detail_no))) {
            redirectTo("formula_detail.php");
        }
    }
}
$stage = $conn->queryRaw("select * from mstage");
$where_1 = $conn->queryRaw("select * from formula_detail where formula_detail_no = '".$formula_detail_no."' ",true);

?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo TITLE_ENG; ?> </title>

    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link href="../bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css"/>
    <link href="../dist/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <link href="../dist/css/AdminLTE.css" rel="stylesheet" type="text/css"/>

    <link href="../dist/css/skins/_all-skins.css" rel="stylesheet" type="text/css"/>
    <link href="../assets/dist/pagination.css" rel="stylesheet">
    <link href="../assets/css/custom.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../assets/css/jquery.datetimepicker.css">
    <style>

        #demo, .paginationjs {
            display: flex;
            flex-direction: row;
            flex-wrap: wrap;
            justify-content: center;
            align-items: center;
        }
    </style>
</head>
<body class="skin-yellow sidebar-mini">
<div class="wrapper">
    <?php include "navbar.php" ?>
    <?php include "sidebar.php" ?>
    <div id="posContain" class="content-wrapper">
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <form class="form-horizontal" id="form_data" name="form_data" method="post"
                          enctype="multipart/form-data">
                        <input id="__cmd" name="__cmd" type="hidden" value="">

                        <div class="col-md-12">
                            <label class="col-sm-3 control-label">
                            </label>
                        </div>
                        <div class="clr"></div>
                        <div class="col-sm-12">
                            <div class="box box-success">
                                <div class="box-header with-border">
                                    <h3 class="box-title">จัดการข้อมูลสรายละเอียดูตร </h3>
                                </div>
                                <div class="box-body">
                                    <input type="hidden" name="__formula_detail_no" id="__formula_detail_no"
                                           class="form-control"
                                           value="0"
                                           required="true" readonly>
                                    <div class="form-group">
                                        <div align="right">
                                            <label class="col-sm-3 control-label">
                                                สูตร :
                                            </label>
                                        </div>
                                        <div class="col-sm-5">
                                            <div class="input-group">
                                                <input type="hidden" name="__formula_id" id="__formula_id"
                                                       class="form-control"
                                                       value=""
                                                       readonly>
                                                <input type="text" name="__formula_name" id="__formula_name"
                                                       class="form-control"
                                                       value="" readonly>
                                                <a href="javascript:goPage('linkhelp.php?__filter=formula&__action=getFormulaById');"
                                                   class="btn btn-default input-group-addon"><i
                                                            class="fa fa-search"></i> </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div align="right">
                                            <label class="col-sm-3 control-label">
                                                ถนน :
                                            </label>
                                        </div>
                                        <div class="col-sm-5">
                                            <div class="input-group">
                                                <input type="hidden" name="__road_number_id" id="__road_number_id"
                                                       class="form-control"
                                                       value=""
                                                       readonly>
                                                <input type="text" name="__road_number_name" id="__road_number_name"
                                                       class="form-control"
                                                       value="" readonly>
                                                <a href="javascript:goPage('linkhelp.php?__filter=road_number&__action=getRoad_numberById');"
                                                   class="btn btn-default input-group-addon"><i
                                                            class="fa fa-search"></i> </a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div align="right">
                                            <label class="col-sm-3 control-label">
                                                Stage :
                                            </label>
                                        </div>
                                        <div class="col-sm-5">
                                            <?php foreach ($stage as $row) { ?>
                                                <div class="radio">
                                                    <label>
                                                        <input type="radio" name="__stage_id" id="__stage_id"
                                                               value="<?php echo $row['stage_id'] ?>"
                                                               <?php if ($row['stage_id'] == $where_1['stage_id']) { ?> checked="" <?php } ?>
                                                        ><?php echo $row['stage_name'] ?>
                                                        (<?php echo $row['stage_detail'] ?>)

                                                    </label>
                                                </div>
                                            <?php } ?>

                                        </div>

                                    </div>
                                    <div class="form-group">
                                        <div align="right">
                                            <label class="col-sm-3 control-label">
                                                Stage Time :
                                            </label>
                                        </div>
                                        <div class="col-sm-5">
                                            <input type="text" name="__stage_time" id="__stage_time"
                                                   class="form-control"
                                                   value="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div align="right">
                                            <label class="col-sm-3 control-label">
                                                ระยะเวลาเริ่มต้น :
                                            </label>
                                        </div>
                                        <div class="col-sm-5">
                                            <input type="text" name="__duration_time_start" id="__duration_time_start"
                                                   class="form-control"
                                                   value="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div align="right">
                                            <label class="col-sm-3 control-label">
                                                ระยะเวลาสิ้นสุด :
                                            </label>
                                        </div>
                                        <div class="col-sm-5">
                                            <input type="text" name="__duration_time_end" id="__duration_time_end"
                                                   class="form-control"
                                                   value="">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div align="right">
                                            <label class="col-sm-3 control-label">
                                            </label>
                                        </div>
                                        <div class="col-sm-5">
                                            <a class="btn btn-success" href="javascript:goSave();">บันทึก</a>
                                            <a class="btn btn-warning" href="javascript:goClear()">เคลียร์</a>
                                            <a class="btn btn-default" href="formula_detail.php">ย้อนกลับ</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </section>
    </div>
</div>
<?php require_once 'javascript.php'; ?>
<!-- Page script -->
<script>
    $('#menu-formula-main').addClass('active');
    $('#menu-formulad').addClass('active');

    function helpReturn(value, action) {
        $.ajax({
            url: 'Allservice.php',
            data: {id: value, action: action},
            method: 'GET',
            success: function (result) {
                var data = JSON.parse(result);
                if (action == "getFormula_detailById") {
                    if (data.formula_detail_no != null) {
                        console.log(data);
                        setValueFormula_detail(data);
                    }
                }
                if (action == "getRoad_numberById") {
                    if (data.road_number_id != null) {
                        console.log(data);
                        setValueRoad_number(data);
                    }
                }
                if (action == "getFormulaById") {
                    if (data.formula_id != null) {
                        console.log(data);
                        setValueFormula(data);
                    }
                }
            }
        });
    }

    function setValueFormula_detail(data) {
        with (document.form_data) {
            $("#__formula_detail_no").val(data.formula_detail_no);
            $("#__duration_time_start").val(data.duration_time_start);
            $("#__duration_time_end").val(data.duration_time_end);

            $("#__road_number_id").val(data.road_number_id);
            $("#__road_number_name").val(data.road_number_name);

            $("#__formula_id").val(data.formula_id);
            $("#__stage_id").prop(data.stage_id );
            $("#__stage_time").val(data.stage_time);
            $("#__formula_name").val(data.formula_name);
        }
    }

    function setValueRoad_number(data) {
        with (document.form_data) {
            $("#__road_number_id").val(data.road_number_id);
            $("#__road_number_name").val(data.road_number_name);

        }
    }

    function setValueFormula(data) {
        with (document.form_data) {
            $("#__formula_id").val(data.formula_id);
            $("#__formula_name").val(data.formula_name);

        }
    }
</script>
<script>helpReturn('<?php echo $formula_detail_no;?>', 'getFormula_detailById')</script>
</body>
</html>


