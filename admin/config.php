<?php
session_start();
require_once "../common.inc.php";
if (!is_list_session(array(ADMIN_LEVEL)))
    redirect_to('index.php');

require_once "../connection.inc.php";

$cmd = getIsset("__cmd");

$config_id = getIsset('__config_id');
if ($cmd == "save") {
    $value = array(
        "email" => getIsset('__email'),
        "address" => getIsset('__address'),
        "phone" => getIsset('__phone'),
        "description" => getIsset('__description'),
    );
    if ($config_id == "") {
        if ($conn->create("config_web", $value)) {
            redirectTo('config.php');
        }

    } else {
        if ($conn->update("config_web", $value, array("config_id" => $config_id))) {
            redirectTo('config.php');
        }
    }
}
$config = $conn->select("config_web", array(), true);
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo TITLE_ENG; ?> </title>

    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link href="../bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css"/>
    <link href="../dist/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <link href="../dist/css/AdminLTE.css" rel="stylesheet" type="text/css"/>

    <link href="../dist/css/skins/_all-skins.css" rel="stylesheet" type="text/css"/>
    <link href="../assets/dist/pagination.css" rel="stylesheet">
    <link href="../assets/css/custom.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../assets/css/jquery.datetimepicker.css">
    <style>

        #demo, .paginationjs {
            display: flex;
            flex-direction: row;
            flex-wrap: wrap;
            justify-content: center;
            align-items: center;
        }
    </style>
</head>
<body class="skin-black sidebar-mini">
<div class="wrapper">
    <?php include "navbar.php" ?>
    <?php include "sidebar.php" ?>
    <div id="posContain" class="content-wrapper">
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <form class="form-horizontal" id="form_data" name="form_data" enctype="multipart/form-data"
                          method="post">
                        <div class="">
                            <input id="__cmd" name="__cmd" type="hidden" value="">
                            <div class="col-md-12">
                                <label class="col-sm-3 control-label">
                                </label>
                            </div>
                            <div class="clr"></div>
                            <div class="col-sm-12">
                                <div class="box box-primary">
                                    <div class="box-header with-border">
                                        <h3 class="box-title">จัดการข้อมูลหน้าเว็บ</h3>
                                    </div>

                                    <div class="box-body">
                                        <input type="hidden" name="__config_id" id="__config_id"
                                               class="form-control"
                                               value="<?php echo $config['config_id'];?>"
                                               required="true" readonly>
                                        <div class="form-group">
                                            <div align="right">
                                                <label class="col-sm-3 control-label">
                                                    อีเมล์ :
                                                </label>
                                            </div>
                                            <div class="col-sm-4">
                                                <input type="email" name="__email" id="__email" class="form-control"
                                                       value="<?php echo $config['email'];?>"
                                                       onblur="trimValue(this);" required="true"
                                                       onkeypress="chkNotThaiChaOnly(event)">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div align="right">
                                                <label class="col-sm-3 control-label">
                                                    เบอร์โทรศัพท์ :
                                                </label>
                                            </div>
                                            <div class="col-sm-3">
                                                <input type="text" name="__phone" id="__phone" class="form-control"
                                                       value="<?php echo $config['phone'];?>"
                                                       onblur="trimValue(this);" maxlength="10"
                                                       onkeypress="chkInteger(event)">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div align="right">
                                                <label class="col-sm-3 control-label">
                                                    ที่อยู่ :
                                                </label>
                                            </div>
                                            <div class="col-sm-6">
                                                <textarea id="__address" name="__address" class="form-control"
                                                          onblur="trimValue(this);" rows="4"><?php echo $config['address'];?></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div align="right">
                                                <label class="col-sm-3 control-label">
                                                    รายละเอียด :
                                                </label>
                                            </div>
                                            <div class="col-sm-6">
                                                <textarea id="__description" name="__description" class="form-control"
                                                          onblur="trimValue(this);" rows="4"><?php echo $config['description'];?></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div align="right">
                                                <label class="col-sm-3 control-label">
                                                </label>
                                            </div>
                                            <div class="col-sm-5">
                                                <a class="btn btn-success" href="javascript:goSave();">บันทึก</a>
                                                <a class="btn btn-warning" href="javascript:goClear()">เคลียร์</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </section>
    </div>
</div>
<?php require_once 'javascript.php'; ?>
<!-- Page script -->
<script>
    $('#menu-config').addClass('active');

</script>
</body>
</html>


