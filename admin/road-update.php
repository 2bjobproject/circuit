<?php
session_start();
require_once "../common.inc.php";
if (!is_list_session(array(ADMIN_LEVEL)))
    redirect_to('index.php');

require_once "../connection.inc.php";

$cmd = getIsset("__cmd");
$test = array();
$road_number_id = getIsset('__road_number_id');
if ($cmd == "save") {
    $value = array(
        "road_number_name" => getIsset('__road_number_name'),
        "default_duration_time" => getIsset('__default_duration_time'),
        "lat_start" => getIsset('__lat_start'),
        "lng_start" => getIsset('__lng_start'),
        "lat_end" => getIsset('__lat_end'),
        "lng_end" => getIsset('__lng_end'),
    );
    if ($road_number_id == "0") {
        if ($conn->create("mroad_number", $value)) {
            redirectTo("road.php");
        }

    } else {
        if ($conn->update("mroad_number", $value, array("road_number_id" => $road_number_id))) {
            redirectTo("road.php");
        }
    }
}

?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo TITLE_ENG; ?> </title>

    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link href="../bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css"/>
    <link href="../dist/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <link href="../dist/css/AdminLTE.css" rel="stylesheet" type="text/css"/>

    <link href="../dist/css/skins/_all-skins.css" rel="stylesheet" type="text/css"/>
    <link href="../assets/dist/pagination.css" rel="stylesheet">
    <link href="../assets/css/custom.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../assets/css/jquery.datetimepicker.css">
    <style>

        #demo, .paginationjs {
            display: flex;
            flex-direction: row;
            flex-wrap: wrap;
            justify-content: center;
            align-items: center;
        }
    </style>
</head>
<body class="skin-yellow sidebar-mini">
<div class="wrapper">
    <?php include "navbar.php" ?>
    <?php include "sidebar.php" ?>
    <div id="posContain" class="content-wrapper">
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <form class="form-horizontal" id="form_data" name="form_data" method="post"
                          enctype="multipart/form-data">
                        <input id="__cmd" name="__cmd" type="hidden" value="">

                        <div class="col-md-12">
                            <label class="col-sm-3 control-label">
                            </label>
                        </div>
                        <div class="clr"></div>
                        <div class="col-sm-12">
                            <div class="box box-success">
                                <div class="box-header with-border">
                                    <h3 class="box-title">จัดการข้อมูลผู้ดูแลระบบ </h3>
                                </div>
                                <div class="box-body">
                                    <input type="hidden" name="__road_number_id" id="__road_number_id" class="form-control"
                                           value="0"
                                           required="true" readonly>
                                    <div class="form-group">
                                        <div align="right">
                                            <label class="col-sm-3 control-label">
                                                ชื่อถนน :
                                            </label>
                                        </div>
                                        <div class="col-sm-5">
                                            <input type="text" name="__road_number_name" id="__road_number_name"
                                                   class="form-control"
                                                   value=""
                                                   onblur="trimValue(this);" required="true">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div align="right">
                                            <label class="col-sm-3 control-label">
                                                เวลามาตรฐาน :
                                            </label>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="input-group">
                                                <input type="text" name="__default_duration_time"
                                                       id="__default_duration_time"
                                                       class="form-control"
                                                       value=""
                                                       readonly required
                                                ><a class="input-group-addon btn-clear-date" href="#"><i
                                                            class="fa fa-calendar"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div align="right">
                                            <label class="col-sm-3 control-label">
                                                ละติจูดเริ่มต้น :
                                            </label>
                                        </div>
                                        <div class="col-sm-5">
                                            <input type="text" name="__lat_start" id="__lat_start"
                                                   class="form-control" 
                                                   value="" onblur="chkInteger(event)" required="true">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div align="right">
                                            <label class="col-sm-3 control-label">
                                                ละติจูดสิ้นสุด :
                                            </label>
                                        </div>
                                        <div class="col-sm-5">
                                            <input type="text" name="__lat_end" id="__lat_end"
                                                   class="form-control"
                                                   value="" onblur="chkInteger(event)" required="true">
                                        </div>

                                    </div>
                                    <div class="form-group">
                                        <div align="right">
                                            <label class="col-sm-3 control-label">
                                                ลองจิจูดเริ่มต้น :
                                            </label>
                                        </div>
                                        <div class="col-sm-5">
                                            <input type="text" name="__lng_start" id="__lng_start"
                                                   class="form-control"
                                                   value="" onblur="chkInteger(event)" required="true">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div align="right">
                                            <label class="col-sm-3 control-label">
                                                ลองจิจูดสิ้นสุด :
                                            </label>
                                        </div>
                                        <div class="col-sm-5">
                                            <input type="text" name="__lng_end" id="__lng_end"
                                                   class="form-control"
                                                   value="" onblur="chkInteger(event)" required="true">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div align="right">
                                            <label class="col-sm-3 control-label">
                                            </label>
                                        </div>
                                        <div class="col-sm-5">
                                            <a class="btn btn-success" href="javascript:goSave();">บันทึก</a>
                                            <a class="btn btn-warning" href="javascript:goClear()">เคลียร์</a>
                                            <a class="btn btn-default" href="road.php">ย้อนกลับ</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </section>
    </div>
</div>
<?php require_once 'javascript.php'; ?>
<!-- Page script -->
<script>
    $('#menu-road-main').addClass('active');
    $('#menu-road').addClass('active');
    $('#__default_duration_time').datetimepicker({
        datepicker: true,
        timepicker: false,
        format: 'Y-m-d H:i:s',
        closeOnDateSelect: true
    });

    function helpReturn(value, action) {
        $.ajax({
            url: 'Allservice.php',
            data: {id: value, action: action},
            method: 'GET',
            success: function (result) {
                var data = JSON.parse(result);
                if (action == "getRoad_numberById") {
                    if (data.road_number_id != null) {
                        console.log(data);
                        setValueRoad_number(data);
                    }
                }
            }
        });
    }

    function setValueRoad_number(data) {
        with (document.form_data) {
            $("#__road_number_id").val(data.road_number_id);
            $("#__road_number_name").val(data.road_number_name);
            $("#__default_duration_time").val(data.default_duration_time);
            $("#__lat_start").val(data.lat_start);
            $("#__lng_start").val(data.lng_start);
            $("#__lat_end").val(data.lat_end);
            $("#__lng_end").val(data.lng_end);
        }
    }
</script>
<script>helpReturn('<?php echo $road_number_id;?>', 'getRoad_numberById')</script>
</body>
</html>


