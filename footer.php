<section id="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h1><?php echo TITLE_ENG;?></h1>
                <p><?php echo $company['description'];?></p>
                <br>
            </div>
            <div class="col-md-6">
                <h1>Contact Us</h1>
                <p><i class="fa fa-map-pin fa-fw"></i> <?php echo $company['address'];?></p>
                <p><i class="fa fa-phone fa-fw"></i> <?php echo $company['phone'];?></p>
                <p><i class="fa fa-envelope fa-fw"></i> <?php echo $company['email'];?></p>
            </div>
        </div>

        <div class="row">
            <hr>
            <div class="col-md-12 text-center bottom">
                <p>Copyright &copy; 2018 <?php echo TITLE_ENG;?>. All rights reserved.</p>
            </div>
        </div>
    </div>
</section>