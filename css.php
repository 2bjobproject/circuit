<!-- FAVICON -->
<link rel="shortcut icon" href="front/html/images/favicon.png"/>

<!-- LOAD MAIN CSS -->
<link href="front/html/style.css" rel="stylesheet" type="text/css">

<!-- LOAD COLOR STYLE (OPTONAL) CSS -->
<link href="front/html/styles/lunar.css" rel="stylesheet" type="text/css" id="colorstyle">

<link href="assets/dist/pagination.css" rel="stylesheet" type="text/css">
<!-- LOAD CUSTOM CSS -->
<link rel="stylesheet" href="plugins/jquery-bar-rating-master/dist/themes/fontawesome-stars.css">
<link href="assets/css/jquery.datetimepicker.css" rel="stylesheet" type="text/css">
<link href="plugins/iCheck/all.css" rel="stylesheet">

<link href="plugins/SmartWizard-master/src/css/smart_wizard.css" rel="stylesheet" type="text/css" />
<link href="plugins/SmartWizard-master/src/css/smart_wizard_theme_arrows.css" rel="stylesheet" type="text/css" />

<link href="front/html/custom.css" rel="stylesheet" type="text/css">
<style>

    #demo, .paginationjs {
        display: flex;
        flex-direction: row;
        flex-wrap: wrap;
        justify-content: center;
        align-items: center;
    }
</style>