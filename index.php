<?php
session_start(); // เปิดใช้งาน session
require_once "common.inc.php"; //
require_once "connection.inc.php"; //
redirectTo('admin/login.php');
$topic_type_id = getIsset("topic_type_id");
if ($topic_type_id == "")
    $topic_type_id = $conn->select("topic_type", array(), true)['topic_type_id'];

$filterDefault = " where topic_type.topic_type_id='$topic_type_id' ";

$keyword = getIsset("keyword");
if ($keyword != "") {
    $filterDefault .= " and topic_name like '%" . $keyword . "%'";
}
$sql = "select topic.*,topic_type_name,status_name from topic
left join topic_type on topic_type.topic_type_id=topic.topic_type_id
left join status on status.status_id=topic.is_active ";
$result_row = $conn->queryRaw($sql . $filterDefault);//คิวรี่ คำสั่ง
$total = sizeof($result_row);
$for_end = $limit;
$for_start = $start * $limit;

$select_all = $conn->queryRaw($sql . $filterDefault . " order by topic_id desc  limit " . $for_start . "," . $for_end);
$total_num = sizeof($select_all);

$topic_types = $conn->queryRaw("select topic_type.*,count(topic_id) as count_topic from topic_type
left join topic on topic_type.topic_type_id=topic.topic_type_id
group by topic_type.topic_type_id");
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- METAS -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- TITLE -->
    <title><?php echo TITLE_ENG; ?></title>
    <?php require_once "css.php"; ?>


</head>

<body>
<!-- Preloader Start -->
<div id="preloader">
    <i class="fa fa-spinner fa-spin preloader-animation" aria-hidden="true"></i>
</div>
<!-- Preloader End -->

<!-- WRAPPER START -->
<div id="wrapper">
    <!-- HEADER START -->
    <?php require_once "menu.php"; ?>
    <!-- HEADER END -->


    <!-- HERO SLIDER -->
    <?php require_once "slider.php"; ?>
    <!-- HERO SLIDER END-->


    <!-- CONTENT START -->
    <section id="content">

        <section id="blog-list" class="container">
            <form class="form-inline" id="form_data" name="form_data" method="post">
                <div class="form-group">
                    <div class="col-sm-9 col-xs-12">
                        <?php
                        $index = $for_start;
                        foreach ($select_all as $row) {
                            $index++;
                            ?>
                            <div class="col-md-12">
                                <div class="blog-item">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="blog-item-inner">
                                                <h3><a ><?php echo $row['topic_name'];?></a></h3>
                                                <p><?php echo $row['remark'];?></p>
                                                <a href="topic-detail.php?topic_id=<?php echo $row['topic_id'];?>" class="btn btn-sm btn-default">READ MORE</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                        <div class="col-md-12">
                            <?php include "pageindex.php" ?>
                        </div>
                    </div>
                    <div class="col-sm-3 col-xs-12">
                        <div class="widget hidden">
                            <div class="input-group">
                                <input type="text" class="form-control input-lg" name="keyword"
                                       value="<?php echo $keyword; ?>" placeholder="Search...">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default btn-lg" type="submit"><i
                                                class="fa fa-search"></i></button>
                                    </span>
                            </div>
                        </div>
                        <div class="widget">
                            <h4>สถานที่ท่องเที่ยว</h4>
                            <ul class="list-unstyled link-list">
                                <?php foreach ($topic_types as $type) { ?>
                                    <li><i class="fa fa-angle-right fa-fw" aria-hidden="true"></i> <a
                                            href="index.php?topic_type_id=<?php echo $type['topic_type_id']; ?>"><?php echo $type['topic_type_name']; ?>
                                            <span
                                                class="pull-right">(<?php echo $type['count_topic']; ?>)</span></a></li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </form>

        </section>

        <div id="partners" class="container-fluid padding35">
            <div class="container">
                <div class="row wow fadeIn">

                </div>
            </div>
        </div>

    </section>
    <!-- CONTENT END -->

    <!-- FOOTER START -->
    <?php require_once "footer.php"; ?>
    <!-- FOOTER END -->


</div>
<!-- WRAPPER END -->

<!-- back to top button -->
<a id="back-to-top" href="#" class="btn btn-primary btn-lg back-to-top" role="button" title="เลื่อนขึ้น"
   data-toggle="tooltip" data-placement="left"><span class="glyphicon glyphicon-chevron-up"></span></a>


<!-- SCRIPT START -->
<?php require_once "script.php"; ?>
<!-- SCRIPT END -->
<script>

</script>
</body>
</html>

