<section id="header">
    <!-- Fixed navbar -->
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                        aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">
                    <h2 style="margin-top: 5px"> <?php echo TITLE_ENG;?></h2>

                    <!--                    <img src="front/html/images/edura-logo.png" alt="Edura - Multipurpose Website Template">-->
                </a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav">
<!--                    <li><a href="index.php">หน้าหลัก</a></li>-->
                    <li><a href="admin/login.php">ผู้ดูแลระบบ</a></li>
                </ul>
            </div><!--/.nav-collapse -->
        </div>
    </nav>
</section>