<?php
require_once('../connection.inc.php');
require_once 'firebaseLib.php';

// --- This is your Firebase URL
$url = 'https://traffic-control-93e25.firebaseio.com/';
// --- Use your token from Firebase here
$token = 'dpSTpMGvYUc4GtTT3fZrJcZcNSaRhyvWy3TALFtK';

$boxes = $conn->queryRaw("SELECT
        mtraffic_box.stage_trigger_field,
        mtraffic_box.time_trigger_field,
        mroad_number.lat_start,
        mroad_number.lat_end,
        mroad_number.lng_start,
        mroad_number.lng_end,
        mroad_number.road_number_id
        FROM
        mroad_number
        LEFT JOIN mtraffic_box ON mroad_number.road_number_id = mtraffic_box.road_number_id
");
foreach ($boxes as $box) {
    $result = GetDrivingDistance($box['lat_start'], $box['lng_start'], $box['lat_end'], $box['lng_end']);
    $formula = $conn->queryRaw("select * from formula_detail 
where road_number_id =" . $box['road_number_id'] . " and " . $result['time']['value'] . " between duration_time_start and duration_time_end", true);
    if ($formula != null) {

        $firebasePath = '/time-traffic/'. $box['time_trigger_field'];
        $fb = new fireBase($url, $token);
        $response = $fb->set($firebasePath, intval($formula['stage_time']));

        $firebasePath = '/time-traffic/'. $box['stage_trigger_field'];
        $fb = new fireBase($url, $token);
        $response = $fb->set($firebasePath, intval($formula['stage_id']));
    }

}


function GetDrivingDistance($lat1, $long1, $lat2, $long2)
{
    $url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=" . $lat1 . "," . $long1 . "&destinations=" . $lat2 . "," . $long2 . "&mode=driving&language=th-TH&key=AIzaSyBA84Lz86jVp2J3kK9j4z1DUA68aJFEtjY";
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    $response = curl_exec($ch);
    curl_close($ch);
    $response_a = json_decode($response, true);
    $dist = $response_a['rows'][0]['elements'][0]['distance'];
    $time = $response_a['rows'][0]['elements'][0]['duration'];

    return array('distance' => $dist, 'time' => $time);
}

?>