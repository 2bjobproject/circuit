<?php
session_start(); // เปิดใช้งาน session
require_once "common.inc.php"; //
require_once "connection.inc.php"; //

$topic_id = getIsset("topic_id");
$sql = "select topic.*,topic_type_name,status_name from topic
left join topic_type on topic_type.topic_type_id=topic.topic_type_id
left join status on status.status_id=topic.is_active
where topic_id='$topic_id'";
$topic = $conn->queryRaw($sql, true);


$topic_types = $conn->queryRaw("select topic_type.*,count(topic_id) as count_topic from topic_type
left join topic on topic_type.topic_type_id=topic.topic_type_id
group by topic_type.topic_type_id");

$sql = "select topic_detail.*,topic_detail_type_name from topic_detail
left join topic_detail_type on topic_detail_type.topic_detail_type_id=topic_detail.topic_detail_type_id
where topic_id='$topic_id'";
$topic_details = $conn->queryRaw($sql);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- METAS -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- TITLE -->
    <title><?php echo TITLE_ENG; ?></title>
    <?php require_once "css.php"; ?>


</head>

<body>
<!-- Preloader Start -->
<div id="preloader">
    <i class="fa fa-spinner fa-spin preloader-animation" aria-hidden="true"></i>
</div>
<!-- Preloader End -->

<!-- WRAPPER START -->
<div id="wrapper">
    <!-- HEADER START -->
    <?php require_once "menu.php"; ?>
    <!-- HEADER END -->


    <!-- HERO SLIDER -->
    <?php require_once "slider.php"; ?>
    <!-- HERO SLIDER END-->


    <!-- CONTENT START -->
    <section id="content">

        <section id="blog-list" class="container">
            <form class="form-inline" id="form_data" name="form_data" method="post">
                <div class="form-group">
                    <div class="col-xs-12">
                        <div class="blog-item">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="blog-item-inner label-content-ckeditor">
                                        <h2><?php echo $topic['topic_name'];?> <a href="index.php" class="btn btn-default pull-right">Back</a></h2>
                                        <p class="lead"><?php echo $topic['remark'];?></p>

                                        <p><?php echo $topic['topic_detail'];?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php foreach($topic_details as $detail){?>
                        <div class="blog-item">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="blog-item-inner label-content-ckeditor">
                                        <h2><?php echo $detail['topic_detail_name'];?></h2>
                                        <p class="lead"><?php echo $detail['remark'];?></p>

                                        <p><?php echo $detail['detail'];?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php }?>
                    </div>
                </div>
            </form>

        </section>

        <div id="partners" class="container-fluid padding35">
            <div class="container">
                <div class="row wow fadeIn">

                </div>
            </div>
        </div>

    </section>
    <!-- CONTENT END -->

    <!-- FOOTER START -->
    <?php require_once "footer.php"; ?>
    <!-- FOOTER END -->


</div>
<!-- WRAPPER END -->

<!-- back to top button -->
<a id="back-to-top" href="#" class="btn btn-primary btn-lg back-to-top" role="button" title="เลื่อนขึ้น"
   data-toggle="tooltip" data-placement="left"><span class="glyphicon glyphicon-chevron-up"></span></a>


<!-- SCRIPT START -->
<?php require_once "script.php"; ?>
<!-- SCRIPT END -->
<script>

</script>
</body>
</html>

